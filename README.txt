DON'T FORGET TO CHECK OUT A BRANCH

Have you ever wanted to add a separator like a comma ',' between the output of taxonomy terms?

When you output a list of terms in a view on Drupal you get something like this:

Topics: Drupal WordPress Development Design Process

So what do you think the topics are? "WordPress Development" "Design Process", or is it "WordPress", "Development", "Design Process".
You can see where I'm going with this.

Wouldn't it be nice to add a separator of any kind to make this easier to read? 

I created a module that adds a Display handler to the Term reference field that allows the output of a separator.

It also allows the addition of a separator at the end in the event that you want to separate multiple vocabularies.
